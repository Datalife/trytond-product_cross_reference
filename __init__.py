# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import sale
from . import stock


def register():
    Pool.register(
        product.ProductCrossReference,
        product.Product,
        module='product_cross_reference', type_='model')
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        module='product_cross_reference', type_='model',
        depends=['sale'])
    Pool.register(
        sale.SaleReport,
        module='product_cross_reference', type_='report',
        depends=['sale'])
    Pool.register(
        stock.DeliveryNote,
        module='product_cross_reference', type_='report',
        depends=['stock'])
