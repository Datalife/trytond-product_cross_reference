# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from decimal import Decimal
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool
from trytond.modules.company.tests import create_company
from trytond.modules.currency.tests import create_currency


class ProductCrossReferenceTestCase(ModuleTestCase):
    """Test Product Cross Reference module"""
    module = 'product_cross_reference'
    extras = ['sale']

    def create_sale(self, product, party):
        pool = Pool()
        Sale = pool.get('sale.sale')

        company = create_company()
        currency = create_currency('cu1')
        sale, = Sale.create([{
            'party': party,
            'company': company,
            'currency': currency,
            'shipment_address': party.addresses[0],
            'lines': [('create', [{
                    'product': product,
                    'quantity': 5.0,
                    'unit': product.sale_uom.id,
                    'unit_price': 2.0
                }])
            ]
        }])

        return sale

    @with_transaction()
    def test0010create_cross_reference(self):
        pool = Pool()
        UOM_Category = pool.get('product.uom.category')
        UOM = pool.get('product.uom')
        Template = pool.get('product.template')
        Party = pool.get('party.party')
        Reference = pool.get('product.cross_reference')
        category, = UOM_Category.create([{'name': 'Test'}])

        party, = Party.create([
            {'name': 'Customer 1',
            'addresses': [('create', [{'name': 'Address 1'}])]
            },
        ])

        party2, = Party.create([
            {'name': 'Customer 2',
            'addresses': [('create', [{'name': 'Address 2'}])]
            },
        ])

        uom, = UOM.create([{
                    'name': 'Test',
                    'symbol': 'T',
                    'category': category.id,
                    'rate': 1.0,
                    'factor': 1.0,
                    }])

        pt1, pt2 = Template.create([
            {'name': 'P1',
             'type': 'goods',
             'list_price': Decimal(20),
             'default_uom': uom.id,
             'salable': True,
             'sale_uom': uom.id,
             'products': [('create', [{'code': '1'}])]},
            {'name': 'P2',
             'type': 'goods',
             'list_price': Decimal(20),
             'default_uom': uom.id,
             'products': [('create', [{'code': '2'}])]}
        ])

        cr1, = Reference.create([{
            'party': party.id,
            'product': pt1.products[0].id,
            'address': party.addresses[0].id,
            'code': '001',
            'name': 'P001'}])

        product, = pt1.products

        pattern = {
            'party': party.id,
            'address': party.addresses[0].id
        }

        # reference matches both party and address fields
        self.assertEqual(product.get_cross_reference(pattern).id, cr1.id)

        # Check reference by sale line
        sale = self.create_sale(product, party)
        self.assertEqual(sale.lines[0].get_cross_reference().id, cr1.id)

        # reference only matches party
        cr1.address = None
        cr1.save()
        self.assertEqual(product.get_cross_reference(pattern).id, cr1.id)

        # reference only matches product
        cr1.party = None
        cr1.save()
        self.assertEqual(product.get_cross_reference(pattern).id, cr1.id)

        # reference has no matches
        cr1.party = party2.id
        cr1.save()
        assert product.get_cross_reference(pattern) is None


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductCrossReferenceTestCase))
    return suite
