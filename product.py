# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (ModelSQL, ModelView, fields, Exclude, MatchMixin,
    sequence_ordered)
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from sql.conditionals import Coalesce
from sql.operators import Equal


class ProductCrossReference(sequence_ordered(), ModelSQL, ModelView,
        MatchMixin):
    """Product Cross reference"""
    __name__ = 'product.cross_reference'

    party = fields.Many2One('party.party', 'Party',
        select=True, ondelete='RESTRICT', depends=['party'])
    address = fields.Many2One('party.address', 'Address',
                              domain=[('party', '=', Eval('party'))],
                              ondelete='RESTRICT', depends=['party'])
    product = fields.Many2One('product.product', 'Product',
        required=True, select=True, ondelete='RESTRICT')
    code = fields.Char('Code')
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(ProductCrossReference, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('party_product_address_exclude', Exclude(t,
                (Coalesce(t.party, -1), Equal),
                (Coalesce(t.address, -1), Equal),
                (t.product, Equal)),
             'product_cross_reference.msg_product_cross_reference_party_product_address_exclude')]

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        table.drop_constraint('party_product_uniq')
        super(ProductCrossReference, cls).__register__(module_name)

        # migrate from 4.2
        table.not_null_action('party', action='remove')

    @fields.depends('product', '_parent_product.name')
    def on_change_product(self):
        if self.product:
            self.name = self.product.name

    @classmethod
    def _get_reference(cls, pattern, base_domain=[]):
        Product = Pool().get('product.product')

        domain = ['OR', ]
        context = pattern.copy() or {}
        match_order = Product.match_cross_reference_order()
        for fieldname in match_order:
            if fieldname:
                context[fieldname] = None
            subdomain = [(key, '=', value) for key, value in context.items()]
            domain.append(subdomain)
        ordering = [(fieldname, 'ASC NULLS LAST')
            for fieldname in match_order[::-1] if fieldname]

        records = cls.search(base_domain + [domain], order=ordering, limit=1)
        if records:
            record, = records
            context = pattern.copy()
            # check it matches
            for to_remove in match_order:
                if to_remove:
                    context[to_remove] = None
                if record.match(context, match_none=True):
                    return record


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    cross_references = fields.One2Many('product.cross_reference', 'product',
        'Cross references')

    @classmethod
    def match_cross_reference_order(cls):
        return [None, 'address', 'party']

    def get_cross_reference(self, pattern):
        context = pattern.copy() or {}
        _pattern = {key_order: context.get(key_order, None)
            for key_order in self.match_cross_reference_order() if key_order}
        for to_remove in self.match_cross_reference_order():
            if to_remove:
                _pattern[to_remove] = None
            reference = self.reference_matches(_pattern)
            if reference:
                return reference

    def reference_matches(self, pattern):
        for reference in self.cross_references:
            if reference.match(pattern, match_none=True):
                return reference
        return False
