# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_cross_reference_pattern(self):
        return {
            'party': self.party.id,
            'address': (
                self.shipment_address and self.shipment_address.id or None)
        }


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def _get_cross_reference_pattern(self):
        return self.sale._get_cross_reference_pattern()

    def get_cross_reference(self):
        if self.product:
            pattern = self._get_cross_reference_pattern()
            return self.product.get_cross_reference(pattern)


class SaleReport(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def product_name(cls, line):
        return cls._get_product_name(line.get_cross_reference(), line.product)

    @classmethod
    def _get_product_name(cls, reference, product):
        if reference:
            return {
                'code': reference.code,
                'name': reference.name,
            }
        return {
            'code': product.code,
            'name': product.name,
        }

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(SaleReport, cls).get_context(
            records, header, data)
        report_context['product_name'] = lambda line: cls.product_name(line)
        return report_context
